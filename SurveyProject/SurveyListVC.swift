//
//  SurveyListVC.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 23/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit
import ObjectMapper

class SurveyListVC: UIViewController , NetworkProtocol , ErrorProtocols , SurveyListProtocol{
    
    var errorVC:ErrorVC!
    
    var offset = 0
    let limit = 5
    
    var surveyListDelg = SurveyListDelegates()
    var array:[SurveyObject] = [SurveyObject]()
    var pagerVC:SurveyPagerVC! = nil
    
    // viewPagerOutlet will be used to add Pager View as child so that its functionality will be on some other controller
    @IBOutlet var tblOutlet: UITableView!
    @IBOutlet var viewPagerOutlet: UIView!
    
    @IBOutlet var viewTopBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // Register for Xib
        // This is required because I am not using prototype from TableView and using a seperate xib and swift file for cell
        // This helps me to reuse cell in some other class when same design cell will be required
        tblOutlet.register(UINib(nibName: "SurveyCell", bundle: nil), forCellReuseIdentifier: "SurveyCell")
        
        // Setting up the delegates
        self.surveyListDelg.delegate = self
        self.tblOutlet.delegate = self.surveyListDelg
        self.tblOutlet.dataSource = self.surveyListDelg
        
        // viewPagerOutlet will be used to add Pager View as child so that its functionality will be on some other controller
        self.callGetSurveyAPI()
        self.addChildForPager()
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        errorVC = storyBoard.instantiateViewController(withIdentifier: "ErrorVC") as! ErrorVC
        errorVC.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - IBActions
    
    @IBAction func reloadBtnAction() {
        
        // Because ishidden mean, Already reloading or having first api call
        if(!self.tblOutlet.isHidden)
        {
            // Resetting the variables to initial stage
            self.offset = 0
            self.array.removeAll()
            self.tblOutlet.reloadData()
            self.pagerVC.totalCount = 0
            self.tblOutlet.isHidden = true
            self.tblOutlet.setContentOffset(CGPoint.zero, animated: true)
            
            // Calling the API to reload the data on Screen
            self.callGetSurveyAPI()
        }
        
    }
    
    // MARK: - Umair Functions
    func ShowAlertMessage(msg:String){
        let alert = UIAlertController(title: Constants.dispayName as? String, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) {
            (result : UIAlertAction) -> Void in
            //self.navigationController?.popToRootViewController(animated: true)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func addChildForPager()
    {
        // viewPagerOutlet will be used to add Pager View as child so that its functionality will be on some other controller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        pagerVC = storyboard.instantiateViewController(withIdentifier: "SurveyPagerVC") as! SurveyPagerVC
        pagerVC.totalCount = 0
        pagerVC.view.frame = self.viewPagerOutlet.bounds
        self.viewPagerOutlet.addSubview(pagerVC.view)
        self.addChildViewController(pagerVC)
    }
    
    // MARK: - API Hit
    func callGetSurveyAPI()  -> Void{
        
        self.surveyListDelg.isLoadingData = true
        
        let url:String = ApiLinks.getSurveyList
        var params = Constants.dictionaryStandard()
        
        params["page"] = String(offset)//String((limit*offset))
        params["per_page"] = String(limit)
        
        let header = Constants.dictionaryStandard()
        
        let obj = NetworkCalls(link:url , notificationName:ApiLinks.getSurveyList, params:params , header:header, addUSerID:true)
        obj.delegate = self
        obj.getAPICall()
    }
    
    // MARK: - APICallBacks
    func apiCallBackWithArray(apiName:String , apiResponse:[AnyObject])
    {
        if(apiName == ApiLinks.getSurveyList)
        {
            for obj in apiResponse{
                do {
                    let data = try JSONSerialization.data(withJSONObject:obj)
                    let dataString = String(data: data, encoding: .utf8)!
                    //print(dataString)
                    
                    let newObj = SurveyObject(JSONString: dataString)
                    array.append(newObj!)
                    
                    
                } catch {
                    
                }
                
            }
            // Passing the required data to UITAbleView delegates so that they would use that data
            self.surveyListDelg.height = self.tblOutlet.frame.size.height
            self.surveyListDelg.array = self.array
            self.tblOutlet.reloadData()
            
            // Unhidding UITableView as it was initially hidden
            self.tblOutlet.isHidden = false
            
            if(apiResponse.count > 0)
            {
                self.surveyListDelg.isLoadingData = false
                offset = offset+1
            }
            
            self.pagerVC.totalCount = self.array.count
        }
    }
    
    func apiCallBack(apiName:String , apiResponse:[String : AnyObject])
    {
        if(apiName == ApiLinks.getSurveyList)
        {
            if apiResponse["message"] is String{
                let message = apiResponse["message"] as! String
                if message == "-11"
                {
                    self.surveyListDelg.isLoadingData = false
                    if(self.tblOutlet.isHidden)
                    {
                        var mainFrame = self.view.frame
                        mainFrame.size.height = mainFrame.size.height - self.viewTopBar.frame.size.height
                        errorVC.view.frame = mainFrame
                        
                        errorVC.errorBtnText = "RETRY"
                        errorVC.errorDetail = "Seems like you are not connected to internet. Please check your internet connection and press refresh button again."
                        errorVC.errorImage = "ntError"
                        
                        errorVC.updateTheValues()
                        self.view.addSubview(errorVC.view)
                    }
                    
                }
            }
        }
    }
    
    // MARK: - SurveyListProtocol
    func moveToQuestionsVC(){
        self.performSegue(withIdentifier: "goToSurveyQuestions", sender: self)
    }
    
    func callAPIForMoreData(){
        self.callGetSurveyAPI()
    }
    
    func changeCurrentSelectedOfChild(pageNumber:Int)
    {
        pagerVC.currentPage = pageNumber
        let indexPath = IndexPath(row: pageNumber, section: 0)
        pagerVC.tblOutlet.scrollToRow(at: indexPath, at: .middle, animated: true)
    }
    
    // MARK: - ErrorProtocols
    func retryActionBtnPressed()
    {
        self.tblOutlet.isHidden = false
        errorVC.view.removeFromSuperview()
        self.reloadBtnAction()
    }
    
    
}
