//
//  SurveyCell.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 23/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit

class SurveyCell: UITableViewCell {

    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var lblDescription: UILabel!
    
    @IBOutlet var btnSurvey: UIButton!
    
    @IBOutlet var imgMain: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.btnSurvey.layer.cornerRadius = CGFloat(self.btnSurvey.frame.size.height/2)
        self.btnSurvey.clipsToBounds = true
    }
    
}
