//
//  NetworkCalls.swift
//  SeekerSwift1
//
//  Created by Mian Umair Nadeem on 11/01/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit
import Alamofire

class NetworkCalls: NSObject {
    
    private var isPost:Bool!
    private var link:String = ""
    private var notificationName:String = ""
    private var params = [String : String]()
    private var header = [String : String]()
    var delegate : NetworkProtocol!
    
    init(link:String ,notificationName:String , params:[String : String] , header:[String : String] , addUSerID:Bool)
    {
        self.link = Constants.baseUrl+link
        self.notificationName = notificationName
        self.params = params
        self.header = header
        
        if(addUSerID){
            self.params["access_token"] = UserDefaults.standard.object(forKey: Constants.kToken) as? String
        }
        
    }
    
    public func getAPICall() ->Void
    {
        print("Link : \(link)")
        print("header : \(header)")
        print("params : \(params)")
        
        if(UserDefaults.standard.bool(forKey: "haveAccessToken")){
            
            Alamofire.request(link, method: .get, parameters: params, headers: header).response
                {
                    response in
                    
                    print("Request: \(String(describing: response.request))")
                    print("Response: \(String(describing: response.response))")
                    print("Error: \(String(describing: response.error))")
                    print("Data: \(String(describing: response.data))")
                    
                    if((response.error) != nil){
                        
                        var responseJSON = Constants.jsonStandard()
                        responseJSON["message"] = "-11" as AnyObject
                        self.delegate.apiCallBack(apiName: self.notificationName, apiResponse: responseJSON)
                        
                        
                        
                    }else{
                        
                        let statusCode = (response.response?.statusCode)!
                        print("         ----------------    The Status Code     --------    \(statusCode)")
                        if statusCode == 403{
                            
                            UserDefaults.standard.set(false, forKey: "haveAccessToken")
                            self.getAccessToken(fromGet: true)
                        }else{
                            
                            do
                            {
                                
                                let responseJSON = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers)
                                // print("The Original Data is :: \(responseJSON)")
                                if responseJSON is NSArray
                                {
                                    self.delegate.apiCallBackWithArray(apiName: self.notificationName, apiResponse: (responseJSON as! [AnyObject]))
                                }else
                                {
                                    self.delegate.apiCallBack(apiName: self.notificationName, apiResponse: (responseJSON as! Constants.jsonStandard))
                                }
                                
                            }
                            catch
                            {
                                print(error)
                            }
                            
                        }
                        
                    }
                    
            }
            
        }else{
            
            // Need to Fetch Access Token
            self.getAccessToken(fromGet: true)
        }
        
        
        
        
 
        
    }
    
    public func postAPICall() ->Void
    {
        
        let request = Alamofire.request(link, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header)
         //   let validation = request.validate()
        print("\(params)")
            request.response
            {
                response in
                
                print("Request: \(String(describing: response.request))")
                print("Response: \(String(describing: response.response))")
                print("Error: \(String(describing: response.error))")
                //print("Data: \(String(describing: response.data))")
                
                if((response.error) != nil){
                    
                    var responseJSON = Constants.jsonStandard()
                    responseJSON["msg"] = "Something went wrong while fetching data from server." as AnyObject
                    let errorString = response.error?.localizedDescription
                    if((errorString?.characters.count)! > 0){
                        responseJSON["message"] = errorString! as AnyObject
                    }
                    self.delegate.apiCallBack(apiName: self.notificationName, apiResponse: responseJSON)
                    
                }else{
                    
                    let statusCode = (response.response?.statusCode)!
                    print("         ----------------    The Status Code     --------    \(statusCode)")
                    
                    do
                    {
                        let responseJSON = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! Constants.jsonStandard
                        //print("The Original Data is :: \(responseJSON)")
                        self.delegate.apiCallBack(apiName: self.notificationName, apiResponse: responseJSON)
                    }
                    catch
                    {
                        print(error)
                    }
                }
                
        }
        
    }
    
    private func getAccessToken(fromGet: Bool)
    {
        var params = Constants.dictionaryStandard()
        
        params["username"] = "carlos@nimbl3.com"
        params["password"] = "antikera"
        params["grant_type"] = "password"
        
        let header = Constants.dictionaryStandard()
        
        let request = Alamofire.request(Constants.baseUrl+ApiLinks.accessToken, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header)
        request.response
            {
                response in
                
                print("Request: \(String(describing: response.request))")
                print("Response: \(String(describing: response.response))")
                print("Error: \(String(describing: response.error))")
                
                if((response.error) != nil){
                    
                    var responseJSON = Constants.jsonStandard()
                    /*
                    responseJSON["msg"] = "Something went wrong while fetching data from server." as AnyObject
                    let errorString = response.error?.localizedDescription
                    if((errorString?.characters.count)! > 0){
                        responseJSON["message"] = errorString! as AnyObject
                    }else{
                        responseJSON["message"] = "-11" as AnyObject
                    }
 */
                   
                    // Error in Access Token
                    responseJSON["message"] = "-11" as AnyObject
                    self.delegate.apiCallBack(apiName: self.notificationName, apiResponse: responseJSON)
                    
                }else{
                    
                    let statusCode = (response.response?.statusCode)!
                    print("         ----------------    The Status Code     --------    \(statusCode)")
                    
                    do
                    {
                        let responseJSON = try JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as! Constants.jsonStandard
                        
                        if responseJSON["access_token"] is String{
                            let accessToken = responseJSON["access_token"] as! String
                            UserDefaults.standard.set(accessToken, forKey: Constants.kToken)
                            self.params["access_token"] = UserDefaults.standard.object(forKey: Constants.kToken) as? String
                            UserDefaults.standard.set(true, forKey: "haveAccessToken")
                            if fromGet{
                                self.getAPICall()
                            }else{
                                self.postAPICall()
                            }
                        }
                        
                    }
                    catch
                    {
                        print(error)
                        var responseJSON = Constants.jsonStandard()
                        responseJSON["message"] = "-11" as AnyObject
                        self.delegate.apiCallBack(apiName: self.notificationName, apiResponse: responseJSON)
                    }
                }
                
        }
    }

}

