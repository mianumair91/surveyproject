//
//  SurveyPagerVC.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 23/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit

class PagerCell: UITableViewCell{
    @IBOutlet var imgCircle: UIImageView!
}

class SurveyPagerVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet var tblOutlet: UITableView!
    
    var currentPage: Int = 0 {
        didSet {
            if((self.tblOutlet) != nil)
            {
                self.tblOutlet.reloadData()
            }
        }
    }
    
    var totalCount: Int = 0 {
        didSet {
            if((self.tblOutlet) != nil)
            {
                self.tblOutlet.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK:- UITAbleView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:PagerCell = tableView.dequeueReusableCell(withIdentifier: "PagerCell") as! PagerCell!
        cell.selectionStyle = .none
        
        if(indexPath.row == currentPage){
            cell.imgCircle.image = UIImage.init(named: "fillCircle")
        }else{
            cell.imgCircle.image = UIImage.init(named: "unFillCircle")
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        //320:163 :: size : X
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }


}
