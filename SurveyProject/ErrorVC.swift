//
//  ErrorVC.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 26/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit

class ErrorVC: UIViewController {
    
    var delegate:ErrorProtocols!
    
    var errorImage:String = ""
    var errorDetail:String = ""
    var errorBtnText:String = ""
    
    @IBOutlet var imgError: UIImageView!
    @IBOutlet var lblError: UILabel!
    @IBOutlet var btnActionError: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - MyFunctions
    func updateTheValues()
    {
        self.imgError.image = UIImage(named: errorImage)
        self.lblError.text = errorDetail
        self.btnActionError.setTitle(errorBtnText, for: .normal)
    }
    
    
    // MARK: - IBActions
    
    @IBAction func actionBtn() {
        self.delegate.retryActionBtnPressed()
    }
    
}
