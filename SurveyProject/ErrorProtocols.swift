//
//  ErrorProtocols.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 26/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit

protocol ErrorProtocols {

    func retryActionBtnPressed()
}
