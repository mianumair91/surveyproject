//
//  SurveyListProtocol.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 23/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import Foundation

// These protocols will help to communicate between SurveyListVC and SurveyListDelegate
// When even SurveyListDelegate want's and action from SurveyListVC, these delegate will be used
protocol SurveyListProtocol{
    
    func moveToQuestionsVC()
    func callAPIForMoreData()
    func changeCurrentSelectedOfChild(pageNumber:Int)
    
}
