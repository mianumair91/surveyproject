//
//  SurveyListDelegates.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 23/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit
import SDWebImage

// This class is implementing the delegates of SurveyListVC
// Implementing delegates will help to use same code for some other UITableView that is showing same data
class SurveyListDelegates: NSObject, UITableViewDelegate, UITableViewDataSource {
    
    var height:CGFloat!
    var array:[SurveyObject] = [SurveyObject]()
    var delegate:SurveyListProtocol!
    
    var total = 0
    var isLoadingData = false
    private var lastScrollPostionY: CGFloat = 0
    var point:CGPoint!
    
    var curIndex = 0
    var lastIndex = 0
    
    // MARK:- UITAbleView Delegates
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (array.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell:SurveyCell = tableView.dequeueReusableCell(withIdentifier: "SurveyCell") as! SurveyCell!
        cell.selectionStyle = .none
        
        DispatchQueue.main.async {
            
            let obj = self.array[indexPath.row]
            cell.lblHeading.text = obj.title
            cell.lblDescription.text = obj.desc
            
            
            var imageName = obj.image// + "l"
            if((imageName?.characters.count)! > 0)
            {
                imageName = imageName! + "l"
                let excapedString = imageName?.replacingOccurrences(of: " ", with: "%20")
                let url = NSURL(string: excapedString!)
                cell.imgMain.sd_setImage(with: url! as URL)
            }
            
        }
        
        cell.btnSurvey.addTarget(self, action: #selector(takeSurveyButtonPressed(button:)), for: .touchUpInside)
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        //320:163 :: size : X
        return height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    // MARK:- ScrollView Delegates
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView){
        
        point = scrollView.contentOffset;
        lastScrollPostionY = scrollView.contentOffset.y;
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        lastScrollPostionY = scrollView.contentOffset.y;
        if(scrollView.contentOffset.y > point.y){
            
            let bottomEdge = scrollView.contentOffset.y + scrollView.frame.size.height
            let LoadedHeight = floor(scrollView.contentSize.height) - (height * 2) - 100
            
            if (bottomEdge >=  LoadedHeight)
            {
                if (!isLoadingData) {
                    
                    isLoadingData=true
                    self.delegate.callAPIForMoreData()
                    
                }
            }
        }
        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        // Calculating the Page number to update the circle
        let pageWidth = scrollView.bounds.size.height
        curIndex = Int (((scrollView.contentOffset.y - pageWidth / 2.0 ) / pageWidth ) + 1)
        if curIndex ==  lastIndex {
            return
        }
        lastIndex = curIndex
        //print("Page Number is :: \(lastIndex)")
        self.delegate.changeCurrentSelectedOfChild(pageNumber: lastIndex)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool){
        //self.delegate.changeCurrentSelectedOfChild(decelerate:decelerate)
    }
    
    // MARK: - IBActions
    func takeSurveyButtonPressed(button: UIButton){
        self.delegate.moveToQuestionsVC()
    }

}
