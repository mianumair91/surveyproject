//
//  SurveyObject.swift
//  SurveyProject
//
//  Created by Mian Umair Nadeem on 23/10/2017.
//  Copyright © 2017 Mian Umair Nadeem. All rights reserved.
//

import UIKit
import ObjectMapper

class SurveyObject: Mappable {
    
    var idd:String!
    var title:String!
    var desc:String!
    var image:String!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        idd    <- map["id"]
        title  <- map["title"]
        desc    <- map["description"]
        image  <- map["cover_image_url"]
    }
    
    func printMySelf() -> Void{
        print("id : \(self.idd)")
        print("title : \(self.title)")
        print("desc : \(self.desc)")
        print("image : \(self.image)")
    }
    
}
